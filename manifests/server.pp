# @summary
#   Configure remctl server
#
# @param ensure
#   Install or remove remctl server.
#
# @param manage_with_systemd
#   True to manage the server with systemd, otherwise xinetd is used.
#
# @param debug
#   Enable debug logging for the remctl server.
#
# @param disable
#   Disable remctl server.
#
# @param krb5_service
#   Specifies which principal is used as the server identity for client
#   authentication (see `remctld(8)` `-s` option). By default, remctld
#   accepts any principal with a key in the specified keytab file.
#   
# @param krb5_keytab
#   Specifies keytab to use as the keytab for server credentials rather
#   than the system default or the value of the KRB5_KTNAME environment
#   variable (see `remctld(8)` `-k` option).
#
# @param port
#   Port where remctl server is listening.
#
# @param user
#   User to run remctl server.
#
# @param group
#   Group to run remctl server.
#
# @param manage_user
#   Should we ensure that `user` and `group` are present / absent.
#   If `manage_user` is set to `true` and user or group is root or zero,
#   this module behaves like if `manage_user` was set to `false`.
#   
# @param only_from
#   List of remote hosts that are allowed to access remctl
#   service (see `xinetd.conf(5)` `only_from` option for format).
#
# @param no_access
#   List of remote hosts that are not allowed to access remctl
#   service (see `xinetd.conf(5)` `no_access` option for format).
#
# @param bind
#   Allows a service to be bound to a specific interface on the
#   machine (see `xinetd.conf(5)` `bind` or `interface` option for format).
#
# @param purge_acls
#   Delete remctl ACL files not controlled by puppet.
#
# @param purge_confs
#   Delete remctl configuration files not controlled by puppet.
#
# @param commands
#   List of [remctl::server::command](#defined-type-remctlservercommand)
#   that will be passed to `create_resources()`.
#
# @param aclfiles
#   List of [remctl::server::aclfile](#defined-type-remctlserveraclfile)
#   that will be passed to `create_resources()`.
#
#
class remctl::server (
    Enum['absent','present'] $ensure = 'present',
    Boolean $manage_with_systemd = false,
    Boolean $debug = $remctl::params::debug,
    Boolean $disable = $remctl::params::disable,
    Optional[String] $krb5_service = 'undef',
    String $krb5_keytab = $remctl::params::krb5_keytab,
    Integer $port = $remctl::params::port,
    String $user = 'root',
    String $group = 'root',
    Boolean $manage_user = false,
    Array[String] $only_from = [ '0.0.0.0/0', '::/0' ],
    Array[String] $no_access = [],
    Optional[String] $bind = undef,
    Boolean $purge_acls = $remctl::params::purge_acls,
    Boolean $purge_confs = $remctl::params::purge_confs,
    String $package_name = $remctl::params::server_package_name,
    Hash $commands = {},
    Hash $aclfiles = {}
) inherits ::remctl::params {

    #
    # Computed values
    #
    $_directories_ensure = $ensure ? { 'present' => 'directory', 'absent' => 'absent' }
    $_files_ensure = $ensure ? { 'present' => 'file', 'absent' => 'absent' }

    if ($port == $remctl::params::port) {
        $_xinetd_service_type = undef
    }
    else {
        $_xinetd_service_type = 'UNLISTED'
    }

    if $debug {
        $_debug = '-d '
    }
    else {
        $_debug = ''
    }

    if $krb5_service != 'undef' {
        $_krb5_service = "-s ${krb5_service} "
    }
    else {
        $_krb5_service = ''
    }

    if $remctl::params::conffile {
        $_conffile = "-f ${remctl::params::conffile} "
    }
    else {
        $_conffile = ''
    }

    if $krb5_keytab {
        $_krb5_keytab = "-k ${krb5_keytab} "
    }
    else {
        $_krb5_keytab = ''
    }

    if $only_from {
        $_only_from = join($only_from, ' ')
    }
    else {
        $_only_from = undef
    }

    if size($no_access) > 0 {
        $_no_access = join($no_access, ' ')
    }
    else {
        $_no_access = undef
    }

    if $disable {
        $_disable = 'yes'
    }
    else {
        $_disable = 'no'
    }

    if $manage_user {

        if $group != 'root' and $group != '0' {
            group { $group:
                ensure      => $ensure,
            }

            $_user_require = [ Group[$group] ]
        }
        else {
            $_user_require = undef
        }

        if $user != 'root' and $user != '0' {
            user { $user:
                ensure  => $ensure,
                comment => 'remctl user',
                gid     => $group,
                require => $_user_require,
                notify  => Package[$package_name]
            }
        }
    }

    if ! defined(Package[$package_name]) {
        package { $package_name:
            ensure => $ensure,
            before => File[$remctl::params::basedir]
        }
    }

    file { $remctl::params::basedir:
        ensure => $_directories_ensure,
        mode   => '0750',
        owner  => $user,
        force  => true,
        group  => $group
    }

    -> file { $remctl::params::confdir:
        ensure  => $_directories_ensure,
        mode    => '0750',
        owner   => $user,
        group   => $group,
        purge   => $purge_confs,
        recurse => $purge_confs,
        force   => $purge_confs,
    }

    -> file { $remctl::params::acldir:
        ensure  => $_directories_ensure,
        mode    => '0750',
        owner   => $user,
        group   => $group,
        purge   => $purge_acls,
        recurse => $purge_acls,
        force   => $purge_acls,
    }

    -> file { $remctl::params::conffile:
        ensure  => $_files_ensure,
        content => template('remctl/remctl.conf'),
        mode    => '0640',
        owner   => $user,
        group   => $group
    }

    # Note(remi):
    # As suggested by Russ A.:
    # - Only update /etc/services if official remctl port was used.
    # - Do not register UDP service anymore as it's very unlikely that
    #   UDP will be used someday.
    -> augeas { 'remctl_etc_services':
        context => '/files/etc/services',
        changes => [
            'defnode remctltcp service-name[.="remctl"][protocol = "tcp"] remctl',
            "set \$remctltcp/port ${remctl::params::port}",
            'set $remctltcp/protocol tcp',
            'set $remctltcp/#comment "remote authenticated command execution"',
        ]
    }

    if $manage_with_systemd {
      # use systemd to manage remctl server
      include systemd

      systemd::socket { 'remctl':
        description   => 'Remctl Listening Socket',
        documentation => [
          'man:remctld(8)',
          'https://www.eyrie.org/~eagle/software/remctl/'
        ],
        listen_stream => ['4373'],
        accept        => true,
        wantedby      => [ 'sockets.target' ],
        require       => File[$remctl::params::confdir, $remctl::params::acldir, $remctl::params::conffile],
      }

      ~> systemd::unit { 'remctl.socket': state => 'active' }

      systemd::service { 'remctl@':
        description    => 'Remctl Per-Connection Server',
        documentation  => [
          'man:remctld(8)',
          'https://www.eyrie.org/~eagle/software/remctld/'
        ],
        execstart      => "${remctl::params::server_bin} ${_krb5_keytab}${_conffile}",
        user           => 'root',
        group          => 'root',
        standard_input => 'socket',
        require        => File[$remctl::params::confdir, $remctl::params::acldir, $remctl::params::conffile],
      }
    } else {
      # use xinetd to manage remctl server
      include xinetd

      xinetd::service { 'remctl':
        ensure       => $ensure,
        port         => $port, # Dupplicate with /etc/services info but xinetd::service requires it
        service_type => $_xinetd_service_type,
        server       => $remctl::params::server_bin,
        server_args  => "${_debug}${_krb5_keytab}${_krb5_service}${_conffile}",
        disable      => $_disable,
        protocol     => 'tcp',
        socket_type  => 'stream',
        user         => $user,
        group        => $group,
        only_from    => $_only_from,
        no_access    => $_no_access,
        bind         => $bind,
        require      => File[$remctl::params::confdir, $remctl::params::acldir, $remctl::params::conffile],
      }
    }

    create_resources('::remctl::server::command', $commands, {})
    create_resources('::remctl::server::aclfile', $aclfiles, {})
}

# vim: tabstop=4 shiftwidth=4 softtabstop=4

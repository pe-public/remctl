#
define remctl::server::command (
  String $command,
  String $subcommand,
  Stdlib::Unixpath $executable,
  Array[String] $acls,
  Hash[String,String] $options = {},
  Enum['present','absent'] $ensure = 'present'
) {

  if ! defined(Class['remctl::server']) {
    fail('You must include the remctl::server class before using any remctl::server::command resources')
  }

  $cmdfile = "${remctl::server::confdir}/${command}"
  $_ensure = (($ensure == 'present') and ($remctl::server::ensure == 'present'))
  $_file_ensure = $_ensure ? { true => 'present', false => 'absent' }


  if (!$acls or size($acls) == 0) {
    fail("Missing acls for commmand '${command}/${subcommand}'")
  }


  if ! defined(Concat[$cmdfile]) {
    concat { $cmdfile:
      ensure => $_file_ensure,
      mode   => '0440',
      force  => false,
      owner  => $remctl::server::user,
      group  => $remctl::server::group,
      warn   => true
    }
  }

  if $_ensure {
    concat::fragment { "${command}_${subcommand}":
        target  => $cmdfile,
        content => template('remctl/server/command.erb')
    }
  }
}

# vim: tabstop=4 shiftwidth=4 softtabstop=4

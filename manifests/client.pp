#
# remctl client class
#
class remctl::client (
    Enum['present','absent'] $ensure = 'present',
    String $package_name = $::remctl::params::client_package_name
) inherits ::remctl::params {

    if ! defined(Package[$package_name]) {
        package { $package_name:
            ensure      => $ensure,
        }
    }
}
